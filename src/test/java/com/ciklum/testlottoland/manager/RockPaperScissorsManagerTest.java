package com.ciklum.testlottoland.manager;

import com.ciklum.testlottoland.model.RockPaperScissorsOption;
import com.ciklum.testlottoland.model.Round;
import com.ciklum.testlottoland.service.RockPaperScissorsGameServiceImpl;
import com.ciklum.testlottoland.service.StatsServiceImpl;
import com.ciklum.testlottoland.vo.PlayResponse;
import com.ciklum.testlottoland.vo.StatsResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Enclosed.class)
public class RockPaperScissorsManagerTest {

    private static PlayResponse buildExpected(String player1, String player2, Round.Result result) {
        return PlayResponse.builder()
                .player1(player1)
                .player2(player2)
                .result(result.toValue())
                .build();
    }

    @RunWith(Parameterized.class)
    public static class TheParameterizedPartForPlay {

        RockPaperScissorsManager underTest;
        private RockPaperScissorsOption fPlayerOne;
        private RockPaperScissorsOption fPlayerTwo;
        private Round.Result fResult;

        public TheParameterizedPartForPlay(RockPaperScissorsOption playerOne, RockPaperScissorsOption playerTwo, Round.Result result) {
            this.fPlayerOne = playerOne;
            this.fPlayerTwo = playerTwo;
            this.fResult = result;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {
                            RockPaperScissorsOption.PAPER,
                            RockPaperScissorsOption.PAPER,
                            Round.Result.DRAW
                    },
                    {
                            RockPaperScissorsOption.PAPER,
                            RockPaperScissorsOption.SCISSORS,
                            Round.Result.PLAYER2WIN
                    },
                    {
                            RockPaperScissorsOption.PAPER,
                            RockPaperScissorsOption.ROCK,
                            Round.Result.PLAYER1WIN
                    },
                    {
                            RockPaperScissorsOption.ROCK,
                            RockPaperScissorsOption.PAPER,
                            Round.Result.PLAYER2WIN
                    },
                    {
                            RockPaperScissorsOption.ROCK,
                            RockPaperScissorsOption.ROCK,
                            Round.Result.DRAW
                    },
                    {
                            RockPaperScissorsOption.ROCK,
                            RockPaperScissorsOption.SCISSORS,
                            Round.Result.PLAYER1WIN
                    },
                    {
                            RockPaperScissorsOption.SCISSORS,
                            RockPaperScissorsOption.PAPER,
                            Round.Result.PLAYER1WIN
                    },
                    {
                            RockPaperScissorsOption.SCISSORS,
                            RockPaperScissorsOption.SCISSORS,
                            Round.Result.DRAW
                    },
                    {
                            RockPaperScissorsOption.SCISSORS,
                            RockPaperScissorsOption.ROCK,
                            Round.Result.PLAYER2WIN
                    }
            });
        }

        @Before
        public void setUp() {
            underTest = new RockPaperScissorsManager(
                    new RockPaperScissorsGameServiceImpl(),
                    new StatsServiceImpl()
            );
        }

        @Test
        public void test() {
            PlayResponse expected = buildExpected(fPlayerOne.toValue(), fPlayerTwo.toValue(), fResult);
            PlayResponse result = underTest.play(fPlayerOne.toValue(), fPlayerTwo.toValue());

            assertEquals(expected.getPlayer1(), result.getPlayer1());
            assertEquals(expected.getPlayer2(), result.getPlayer2());
            assertEquals(expected.getResult(), result.getResult());
        }
    }

    public static class NotParameterizedPart {

        RockPaperScissorsManager underTest;

        @Before
        public void setUp() {
            underTest = new RockPaperScissorsManager(
                    new RockPaperScissorsGameServiceImpl(),
                    new StatsServiceImpl()
            );
        }

        @Test
        public void getOptions() {
            String[] options = underTest.getOptions();
            assertEquals(RockPaperScissorsOption.values().length, options.length);
            Arrays.stream(options).forEach(s -> {
                assertNotNull(RockPaperScissorsOption.forValue(s));
            });
        }

        @Test
        public void getStats() {
            StatsServiceImpl statsService = new StatsServiceImpl();
            underTest = new RockPaperScissorsManager(
                    new RockPaperScissorsGameServiceImpl(),
                    statsService
            );

            // Register Player 1 wins
            statsService.register(new Round(RockPaperScissorsOption.ROCK, RockPaperScissorsOption.SCISSORS));
            // Register Player 2 wins
            statsService.register(new Round(RockPaperScissorsOption.SCISSORS, RockPaperScissorsOption.ROCK));
            // Register draw
            statsService.register(new Round(RockPaperScissorsOption.PAPER, RockPaperScissorsOption.PAPER));

            StatsResponse statsResponse = underTest.getStats();

            assertEquals(3, statsResponse.getTotalRounds());
            assertEquals(1, statsResponse.getTotalWinsPlayerOne());
            assertEquals(1, statsResponse.getTotalWinsPlayerTwo());
            assertEquals(1, statsResponse.getTotalDraws());

        }

    }
}