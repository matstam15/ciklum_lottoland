package com.ciklum.testlottoland.controller;

import com.ciklum.testlottoland.model.RockPaperScissorsOption;
import com.ciklum.testlottoland.model.Round;
import com.ciklum.testlottoland.service.StatsServiceImpl;
import com.ciklum.testlottoland.vo.PlayRequest;
import com.ciklum.testlottoland.vo.PlayResponse;
import com.ciklum.testlottoland.vo.StatsResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class RockPaperScissorsControllerTest {

    @Autowired
    StatsServiceImpl statsService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void play() throws Exception {

        String player1 = RockPaperScissorsOption.PAPER.toValue();
        String player2 = RockPaperScissorsOption.ROCK.toValue();

        PlayRequest payload = new PlayRequest(player1, player2);

        MvcResult mvcResult = mockMvc.perform(post("/rps/play")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(payload)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.player1", CoreMatchers.is(player1)))
                .andExpect(jsonPath("$.player2", CoreMatchers.is(player2)))
                .andExpect(jsonPath("$.result", CoreMatchers.is(1)))
                .andReturn();

        //json response body is converted/mapped to the Java Object
        String jsonResponse = mvcResult.getResponse().getContentAsString();
        PlayResponse response = new ObjectMapper().readValue(jsonResponse, PlayResponse.class);

        assertNotNull(response);
        assertEquals(player1, response.getPlayer1());
        assertEquals(player2, response.getPlayer2());
        assertEquals(1, response.getResult());
    }


    @Test
    public void play_IllegalArguments_1() throws Exception {

        Map<String, Object> expectedResponse = new HashMap<>();
        expectedResponse.put("message", "The request contains errors");
        expectedResponse.put("errors", "Player2 is empty or has an incorrect value\n");

        String player1 = RockPaperScissorsOption.ROCK.toValue();
        String player2 = "SPIKE";
        PlayRequest payload = new PlayRequest(player1, player2);

        mockMvc.perform(post("/rps/play")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(payload)))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", CoreMatchers.is(expectedResponse.get("message"))))
                .andExpect(jsonPath("$.errors", CoreMatchers.is(expectedResponse.get("errors"))))
                .andReturn();
    }


    @Test
    public void play_IllegalArguments_2() throws Exception {

        Map<String, Object> expectedResponse = new HashMap<>();
        expectedResponse.put("message", "The request contains errors");
        expectedResponse.put("errors", "Player1 is empty or has an incorrect value\n");

        String player1 = "SPIKE";
        String player2 = RockPaperScissorsOption.ROCK.toValue();
        PlayRequest payload = new PlayRequest(player1, player2);

        mockMvc.perform(post("/rps/play")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(payload)))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", CoreMatchers.is(expectedResponse.get("message"))))
                .andExpect(jsonPath("$.errors", CoreMatchers.is(expectedResponse.get("errors"))))
                .andReturn();
    }

    @Test
    public void play_IllegalArguments_3() throws Exception {

        Map<String, Object> expectedResponse = new HashMap<>();
        expectedResponse.put("message", "The request contains errors");
        expectedResponse.put("errors", "Player1 is empty or has an incorrect value\nPlayer2 is empty or has an incorrect value\n");

        String player1 = "SPIKE";
        String player2 = "DIAMOND";
        PlayRequest payload = new PlayRequest(player1, player2);

        mockMvc.perform(post("/rps/play")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(payload)))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", CoreMatchers.is(expectedResponse.get("message"))))
                .andExpect(jsonPath("$.errors", CoreMatchers.is(expectedResponse.get("errors"))))
                .andReturn();
    }

    @Test
    public void play_emptyPayload() throws Exception {

        Map<String, Object> expectedResponse = new HashMap<>();
        expectedResponse.put("message", "The request contains errors");
        expectedResponse.put("errors", "Empty payload");

        PlayRequest payload = new PlayRequest(null, null);

        mockMvc.perform(post("/rps/play")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(payload)))
                .andExpect(status().is4xxClientError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.message", CoreMatchers.is(expectedResponse.get("message"))))
                .andExpect(jsonPath("$.errors", CoreMatchers.is(expectedResponse.get("errors"))))
                .andReturn();
    }

    @Test
    public void getOptions() throws Exception {

        RockPaperScissorsOption.values();
        String[] response = {"ROCK", "PAPER", "SCISSORS"};
        mockMvc.perform(get("/rps/options")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*", hasSize(response.length)))
                .andExpect(jsonPath("$[0]", CoreMatchers.is(response[0])))
                .andExpect(jsonPath("$[1]", CoreMatchers.is(response[1])))
                .andExpect(jsonPath("$[2]", CoreMatchers.is(response[2])))
                .andReturn();
    }

    @Test
    public void getStats() throws Exception {

        statsService.reset();
        statsService.register(new Round(RockPaperScissorsOption.ROCK, RockPaperScissorsOption.PAPER));
        statsService.register(new Round(RockPaperScissorsOption.SCISSORS, RockPaperScissorsOption.SCISSORS));

        MvcResult mvcResult = mockMvc.perform(get("/rps/stats")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        //json response body is converted/mapped to the Java Object
        String jsonResponse = mvcResult.getResponse().getContentAsString();
        StatsResponse response = new ObjectMapper().readValue(jsonResponse, StatsResponse.class);

        assertEquals(2, response.getTotalRounds());
        assertEquals(1, response.getTotalDraws());
        assertEquals(0, response.getTotalWinsPlayerOne());
        assertEquals(1, response.getTotalWinsPlayerTwo());

    }

}