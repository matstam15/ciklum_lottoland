package com.ciklum.testlottoland.model;

import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class RockPaperScissorsOptionTest {

    @Test
    public void rockBeatsScissors() {
        assertTrue(RockPaperScissorsOption.ROCK.versus(RockPaperScissorsOption.SCISSORS) > 0);
        assertTrue(RockPaperScissorsOption.SCISSORS.versus(RockPaperScissorsOption.ROCK) < 0);
    }

    @Test
    public void scissorsBeatsPaper() {
        assertTrue(RockPaperScissorsOption.SCISSORS.versus(RockPaperScissorsOption.PAPER) > 0);
        assertTrue(RockPaperScissorsOption.PAPER.versus(RockPaperScissorsOption.SCISSORS) < 0);
    }

    @Test
    public void paperBeatsRock() {
        assertTrue(RockPaperScissorsOption.PAPER.versus(RockPaperScissorsOption.ROCK) > 0);
        assertTrue(RockPaperScissorsOption.ROCK.versus(RockPaperScissorsOption.PAPER) < 0);
    }

    @Test
    public void paperDrawPaper() {
        assertEquals(0, RockPaperScissorsOption.PAPER.versus(RockPaperScissorsOption.PAPER));
    }

    @Test
    public void rockDrawRock() {
        assertEquals(0, RockPaperScissorsOption.ROCK.versus(RockPaperScissorsOption.ROCK));
    }

    @Test
    public void scissorsDrawScissors() {
        assertEquals(0, RockPaperScissorsOption.SCISSORS.versus(RockPaperScissorsOption.SCISSORS));
    }

    @Test
    public void testToValue() {
        assertEquals("SCISSORS", Objects.requireNonNull(RockPaperScissorsOption.SCISSORS.toValue()).toUpperCase());
        assertEquals("PAPER", Objects.requireNonNull(RockPaperScissorsOption.PAPER.toValue()).toUpperCase());
        assertEquals("ROCK", Objects.requireNonNull(RockPaperScissorsOption.ROCK.toValue()).toUpperCase());
    }

    @Test
    public void testForValue() {
        assertEquals(RockPaperScissorsOption.SCISSORS, RockPaperScissorsOption.forValue("SCISSORS"));
        assertEquals(RockPaperScissorsOption.PAPER, RockPaperScissorsOption.forValue("PAPER"));
        assertEquals(RockPaperScissorsOption.ROCK, RockPaperScissorsOption.forValue("ROCK"));
        assertNull(RockPaperScissorsOption.forValue("DOES_NOT_EXISTS"));

    }

}