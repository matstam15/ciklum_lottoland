package com.ciklum.testlottoland.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(Enclosed.class)
public class RoundTest {

    @RunWith(Parameterized.class)
    public static class TheParameterizedPart {


        private RockPaperScissorsOption fPlayerOne;
        private RockPaperScissorsOption fPlayerTwo;
        private Round.Result fResult;

        public TheParameterizedPart(RockPaperScissorsOption playerOne, RockPaperScissorsOption playerTwo, Round.Result result) {
            this.fPlayerOne = playerOne;
            this.fPlayerTwo = playerTwo;
            this.fResult = result;
        }

        @Parameterized.Parameters
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {
                            RockPaperScissorsOption.PAPER,
                            RockPaperScissorsOption.PAPER,
                            Round.Result.DRAW
                    },
                    {
                            RockPaperScissorsOption.PAPER,
                            RockPaperScissorsOption.SCISSORS,
                            Round.Result.PLAYER2WIN
                    },
                    {
                            RockPaperScissorsOption.PAPER,
                            RockPaperScissorsOption.ROCK,
                            Round.Result.PLAYER1WIN
                    },
                    {
                            RockPaperScissorsOption.ROCK,
                            RockPaperScissorsOption.PAPER,
                            Round.Result.PLAYER2WIN
                    },
                    {
                            RockPaperScissorsOption.ROCK,
                            RockPaperScissorsOption.ROCK,
                            Round.Result.DRAW
                    },
                    {
                            RockPaperScissorsOption.ROCK,
                            RockPaperScissorsOption.SCISSORS,
                            Round.Result.PLAYER1WIN
                    },
                    {
                            RockPaperScissorsOption.SCISSORS,
                            RockPaperScissorsOption.PAPER,
                            Round.Result.PLAYER1WIN
                    },
                    {
                            RockPaperScissorsOption.SCISSORS,
                            RockPaperScissorsOption.SCISSORS,
                            Round.Result.DRAW
                    },
                    {
                            RockPaperScissorsOption.SCISSORS,
                            RockPaperScissorsOption.ROCK,
                            Round.Result.PLAYER2WIN
                    }
            });
        }

        @Test
        public void test() {
            assertEquals(fResult, new Round(fPlayerOne, fPlayerTwo).getResult());
        }
    }

    public static class NotParameterizedPart {
        @Test
        public void testToValue() {
            Assert.assertEquals(0, Round.Result.DRAW.toValue());
            Assert.assertEquals(1, Round.Result.PLAYER1WIN.toValue());
            Assert.assertEquals(2, Round.Result.PLAYER2WIN.toValue());
        }

        @Test
        public void testForValue() {
            Assert.assertEquals(Round.Result.DRAW, Round.Result.forValue(0));
            Assert.assertEquals(Round.Result.PLAYER1WIN, Round.Result.forValue(1));
            Assert.assertEquals(Round.Result.PLAYER2WIN, Round.Result.forValue(2));
            assertNull(Round.Result.forValue(6));

        }
    }
}