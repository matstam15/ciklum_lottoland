package com.ciklum.testlottoland.service;

import com.ciklum.testlottoland.model.RockPaperScissorsOption;
import com.ciklum.testlottoland.model.Round;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StatsServiceTest {

    StatsServiceImpl underTests;

    @Before
    public void setUp() {
        underTests = new StatsServiceImpl();
    }

    @Test
    public void register_empty() {

        assertEquals(0, underTests.getTotalPlays());
        assertEquals(0, underTests.getWinsByPlayerOne());
        assertEquals(0, underTests.getWinsByPlayerTwo());
        assertEquals(0, underTests.getDraws());

    }

    @Test
    public void register_singleRound() {

        Round round = new Round(RockPaperScissorsOption.SCISSORS, RockPaperScissorsOption.PAPER);
        underTests.register(round);

        assertEquals(1, underTests.getTotalPlays());
        assertEquals(1, underTests.getWinsByPlayerOne());
        assertEquals(0, underTests.getWinsByPlayerTwo());
        assertEquals(0, underTests.getDraws());

    }

    @Test
    public void register_20Round() {

        // consider testing concurrency

        // 20 rounds win by Player 1
        Round winByPlayerOne = new Round(RockPaperScissorsOption.SCISSORS, RockPaperScissorsOption.PAPER);
        for (int i = 0; i < 20; i++)
            underTests.register(winByPlayerOne);

        // 2 rounds win by player 2
        Round winByPlayerTwo = new Round(RockPaperScissorsOption.SCISSORS, RockPaperScissorsOption.ROCK);
        underTests.register(winByPlayerTwo);
        underTests.register(winByPlayerTwo);

        // 1 draws
        Round draw = new Round(RockPaperScissorsOption.ROCK, RockPaperScissorsOption.ROCK);
        underTests.register(draw);

        assertEquals(23, underTests.getTotalPlays());
        assertEquals(20, underTests.getWinsByPlayerOne());
        assertEquals(2, underTests.getWinsByPlayerTwo());
        assertEquals(1, underTests.getDraws());

    }

}