package com.ciklum.testlottoland.service;

import com.ciklum.testlottoland.model.RockPaperScissorsOption;
import com.ciklum.testlottoland.model.Round;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

public class RockPaperScissorsGameServiceImplTest {

    GameService<RockPaperScissorsOption, Round> underTests;

    @Before
    public void setUp() {
        this.underTests = new RockPaperScissorsGameServiceImpl();
    }

    @Test
    public void playRound() {

        Round played = underTests.play(RockPaperScissorsOption.PAPER, RockPaperScissorsOption.PAPER);
        assertEquals(Round.Result.DRAW, played.getResult());
        assertEquals(RockPaperScissorsOption.PAPER, played.getPlayerOneSelection());
        assertEquals(RockPaperScissorsOption.PAPER, played.getPlayerTwoSelection());

        played = underTests.play(RockPaperScissorsOption.PAPER, RockPaperScissorsOption.ROCK);
        assertEquals(Round.Result.PLAYER1WIN, played.getResult());
        assertEquals(RockPaperScissorsOption.PAPER, played.getPlayerOneSelection());
        assertEquals(RockPaperScissorsOption.ROCK, played.getPlayerTwoSelection());

        played = underTests.play(RockPaperScissorsOption.SCISSORS, RockPaperScissorsOption.ROCK);
        assertEquals(Round.Result.PLAYER2WIN, played.getResult());
        assertEquals(RockPaperScissorsOption.SCISSORS, played.getPlayerOneSelection());
        assertEquals(RockPaperScissorsOption.ROCK, played.getPlayerTwoSelection());

    }

    @Test
    public void getOptions() {
        List<RockPaperScissorsOption> expected =
                Arrays.stream(RockPaperScissorsOption.values())
                        .collect(Collectors.toList());


        List<RockPaperScissorsOption> actual = underTests.getOptions();

        assertEquals(expected.size(), actual.size());
        assertTrue(expected.containsAll(actual));
        assertTrue(actual.containsAll(expected));

    }
}