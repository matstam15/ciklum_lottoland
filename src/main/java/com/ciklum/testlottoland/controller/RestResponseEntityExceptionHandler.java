package com.ciklum.testlottoland.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class RestResponseEntityExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseEntity<Map> handleValidationFailure(IllegalArgumentException ex) {
        final Map<String, Object> response = new HashMap<>();
        response.put("message", "The request contains errors");
        response.put("errors", ex.getMessage());
        return ResponseEntity.badRequest().body(response);
    }

    @ExceptionHandler(value = {RuntimeException.class})
    public ResponseEntity<Map> handleRuntimeException(RuntimeException ex) {

        final Map<String, Object> response = new HashMap<>();
        response.put("message", "Internal Error");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }

}