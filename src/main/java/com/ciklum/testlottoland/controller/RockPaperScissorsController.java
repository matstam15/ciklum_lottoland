package com.ciklum.testlottoland.controller;

import com.ciklum.testlottoland.manager.RockPaperScissorsManager;
import com.ciklum.testlottoland.model.RockPaperScissorsOption;
import com.ciklum.testlottoland.vo.PlayRequest;
import com.ciklum.testlottoland.vo.PlayResponse;
import com.ciklum.testlottoland.vo.StatsResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * REST API that exposes the necessary service to play a Rock, Scissors, Paper match.
 * Also provides global stats
 */
@RestController
@RequestMapping("/rps")
@CrossOrigin(origins = {"http://localhost:3000"})
public class RockPaperScissorsController {

    private RockPaperScissorsManager rockPaperScissorsManager;

    @Autowired
    public RockPaperScissorsController(
            RockPaperScissorsManager rockPaperScissorsManager) {
        this.rockPaperScissorsManager = rockPaperScissorsManager;
    }

    /**
     * Evaluates both player selection round
     *
     * @param payload contains the options selected for each player
     * @return a ResponseEntity wrapping a PlayResponse {@link PlayResponse}
     */
    @PostMapping(
            value = "/play",
            produces = "application/json; charset=UTF-8")
    public ResponseEntity<PlayResponse> play(@RequestBody PlayRequest payload) {

        validatePlayRequestPayload(payload);

        return ResponseEntity.ok()
                .body(rockPaperScissorsManager.play(payload.getPlayer1(), payload.getPlayer2()));
    }

    /**
     * Generates a list of eligible options
     *
     * @return a ResponseEntity wrapping an Array with all the option {@link RockPaperScissorsOption}
     */
    @GetMapping(
            value = "/options",
            produces = "application/json; charset=UTF-8")
    public ResponseEntity<String[]> getOptions() {
        return ResponseEntity.ok()
                .body(rockPaperScissorsManager.getOptions());
    }

    /**
     * Generates the global stats of each and every round played by all player
     * (since the last system restart)
     *
     * @return a ResponseEntity wrapping a StatsResponse {@link StatsResponse}
     */
    @GetMapping(value = "/stats",
            produces = "application/json; charset=UTF-8")
    public ResponseEntity<StatsResponse> getSessionStats() {
        return ResponseEntity.ok().body(rockPaperScissorsManager.getStats());
    }

    private void validatePlayRequestPayload(PlayRequest payload) {
        if (payload == null || (payload.getPlayer1() == null && payload.getPlayer2() == null))
            throw new IllegalArgumentException("Empty payload");

        StringBuffer errors = new StringBuffer();
        if (RockPaperScissorsOption.forValue(payload.getPlayer1()) == null)
            errors.append("Player1 is empty or has an incorrect value").append("\n");

        if (RockPaperScissorsOption.forValue(payload.getPlayer2()) == null) {
            errors.append("Player2 is empty or has an incorrect value").append("\n");
        }
        if (StringUtils.isNotBlank(errors))
            throw new IllegalArgumentException(errors.toString());
    }
}
