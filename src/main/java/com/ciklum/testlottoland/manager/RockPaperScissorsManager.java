package com.ciklum.testlottoland.manager;

import com.ciklum.testlottoland.model.RockPaperScissorsOption;
import com.ciklum.testlottoland.model.Round;
import com.ciklum.testlottoland.service.GameService;
import com.ciklum.testlottoland.service.StatsService;
import com.ciklum.testlottoland.vo.PlayResponse;
import com.ciklum.testlottoland.vo.StatsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Encapsulates the business logic and orquestrates communication between services and controllers.
 */
@Component
public class RockPaperScissorsManager {

    private GameService<RockPaperScissorsOption, Round> rockPaperScissorsService;
    private StatsService<Round> statsService;

    @Autowired
    public RockPaperScissorsManager(
            GameService<RockPaperScissorsOption, Round> rockPaperScissorsService,
            StatsService<Round> statsService) {
        this.rockPaperScissorsService = rockPaperScissorsService;
        this.statsService = statsService;
    }

    public PlayResponse play(String player1, String player2) {
        RockPaperScissorsOption player1Choice = RockPaperScissorsOption.forValue(player1);
        RockPaperScissorsOption player2Choice = RockPaperScissorsOption.forValue(player2);
        Round round = rockPaperScissorsService.play(player1Choice, player2Choice);
        statsService.register(round);
        return PlayResponse.builder()
                .player1(round.getPlayerOneSelection().name())
                .player2(round.getPlayerTwoSelection().name())
                .result(round.getResult().toValue())
                .build();
    }

    public String[] getOptions() {
        String[] options = new String[RockPaperScissorsOption.values().length];
        options[0] = RockPaperScissorsOption.ROCK.name();
        options[1] = RockPaperScissorsOption.PAPER.name();
        options[2] = RockPaperScissorsOption.SCISSORS.name();
        return options;
    }

    public StatsResponse getStats() {
        return StatsResponse.builder()
                .totalRounds(statsService.getTotalPlays())
                .totalWinsPlayerOne(statsService.getWinsByPlayerOne())
                .totalWinsPlayerTwo(statsService.getWinsByPlayerTwo())
                .totalDraws(statsService.getDraws())
                .build();
    }
}
