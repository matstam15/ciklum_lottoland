package com.ciklum.testlottoland.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Represent a Round played by Player 1 and Player 2
 */
@Getter
public class Round {

    private RockPaperScissorsOption playerOneSelection;
    private RockPaperScissorsOption playerTwoSelection;
    private Round.Result result;

    public Round(final RockPaperScissorsOption playerOneSelection, final RockPaperScissorsOption playerTwoSelection) {
        this.playerOneSelection = playerOneSelection;
        this.playerTwoSelection = playerTwoSelection;
        result = translateResult(this.playerOneSelection.versus(this.playerTwoSelection));
    }

    /**
     * Translate the result from the semantic of this vs other, to a cardinal
     * relation to the player 1 or 2.
     *
     * @param result the original subjective result
     * @return an objective/cardinal result
     * 0: draw
     * 1: Player 1
     * 2: Player 2
     */
    private static Round.Result translateResult(int result) {
        if (result == 0)
            return Result.DRAW;
        return result == 1 ? Result.PLAYER1WIN : Result.PLAYER2WIN;
    }

    public enum Result {
        DRAW(0), PLAYER1WIN(1), PLAYER2WIN(2);

        private static Map<Integer, Round.Result> namesMap = new HashMap<>(Result.values().length);

        static {
            namesMap.put(DRAW.value, DRAW);
            namesMap.put(PLAYER1WIN.value, PLAYER1WIN);
            namesMap.put(PLAYER2WIN.value, PLAYER2WIN);
        }

        private int value;

        Result(int value) {
            this.value = value;
        }

        @JsonCreator
        public static Result forValue(int value) {
            return namesMap.get(value);
        }

        public int toValue() {
            for (Map.Entry<Integer, Round.Result> entry : namesMap.entrySet()) {
                if (entry.getValue() == this)
                    return entry.getKey();
            }
            throw new IllegalArgumentException();
        }
    }

}
