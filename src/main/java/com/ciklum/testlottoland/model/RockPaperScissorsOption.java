package com.ciklum.testlottoland.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Represent the eligible options to be evaluated during a Round
 */
public enum RockPaperScissorsOption {

    ROCK("ROCK"), PAPER("PAPER"), SCISSORS("SCISSORS");

    private static Map<String, RockPaperScissorsOption> namesMap
            = new HashMap<>(RockPaperScissorsOption.values().length);

    static {
        namesMap.put(StringUtils.upperCase(ROCK.name), ROCK);
        namesMap.put(StringUtils.upperCase(SCISSORS.name), SCISSORS);
        namesMap.put(StringUtils.upperCase(PAPER.name), PAPER);
    }

    private String name;

    RockPaperScissorsOption(String name) {
        this.name = name;
    }

    @JsonCreator
    public static RockPaperScissorsOption forValue(String value) {
        if (StringUtils.isBlank(value))
            throw new IllegalArgumentException();
        return namesMap.get(StringUtils.upperCase(value.trim()));
    }

//    /**
//     * Pick a random value of the RockPaperScissorsOption enum.
//     *
//     * @return a random RockPaperScissorsOption.
//     */
//    public static RockPaperScissorsOption random() {
//        Random random = new Random();
//        return values()[random.nextInt(values().length)];
//    }

    public String toValue() {
        for (Map.Entry<String, RockPaperScissorsOption> entry : namesMap.entrySet()) {
            if (entry.getValue() == this)
                return entry.getKey();
        }
        return null;
    }

    /**
     * Evaluates current value versus other value
     *
     * @param other
     * @return -1 if this one lose
     * 0 if its a draw
     * 1 if other one wins
     */
    public int versus(RockPaperScissorsOption other) {

        int draw = 0;
        int lose = -1;
        int win = 1;

        switch (this) {
            case PAPER:
                switch (other) {
                    case PAPER:
                        return draw;
                    case SCISSORS:
                        return lose;
                    case ROCK:
                        return win;
                }

            case SCISSORS:
                switch (other) {
                    case PAPER:
                        return win;
                    case SCISSORS:
                        return draw;
                    case ROCK:
                        return lose;
                }

            case ROCK:
                switch (other) {
                    case PAPER:
                        return lose;
                    case SCISSORS:
                        return win;
                    case ROCK:
                        return draw;
                }
                break;
        }

        throw new IllegalArgumentException();

    }
}
