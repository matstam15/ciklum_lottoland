package com.ciklum.testlottoland.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Acts as response vo/body for a GET /stats REST Service
 * Represent the current stats for all rounds made since the system is up.
 */
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class StatsResponse {

    /**
     * Total rounds played so far
     */
    private int totalRounds;

    /**
     * Total wins by player 1 so far
     */
    private int totalWinsPlayerOne;

    /**
     * Total wins by player 2 so far
     */
    private int totalWinsPlayerTwo;

    /**
     * Total draws so far
     */
    private int totalDraws;

}
