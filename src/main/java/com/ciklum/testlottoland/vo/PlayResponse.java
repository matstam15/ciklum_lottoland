package com.ciklum.testlottoland.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Acts as response vo/body for a POST /play REST Service
 * Represent the result of the evaluation of a game round.
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlayResponse {

    /**
     * Selection made by Player 1
     */
    private String player1;

    /**
     * Selection made by Player 2
     */
    private String player2;

    /**
     * the result:
     * 0: is a draw
     * 1: Player 1 wins
     * 2: Player 2 wins
     */
    private int result;

}
