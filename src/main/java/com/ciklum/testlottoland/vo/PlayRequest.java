package com.ciklum.testlottoland.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Acts as request vo/body for a POST /play REST Service
 * Represent Player 1 and Player 2 selection's to be evaluated on a game round.
 */
@Getter
@AllArgsConstructor
public class PlayRequest {

    /**
     * Selection made by Player 1
     */
    private String player1;

    /**
     * Selection made by Player 2
     */
    private String player2;
}