package com.ciklum.testlottoland.service;

import java.util.List;

public interface GameService<O, R> {

    R play(O option1, O option2);

    List<O> getOptions();
}
