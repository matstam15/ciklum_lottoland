package com.ciklum.testlottoland.service;

import com.ciklum.testlottoland.model.Round;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class StatsServiceImpl implements StatsService<Round> {

    private AtomicInteger winsByPlayerOne;
    private AtomicInteger winsByPlayerTwo;
    private AtomicInteger draws;

    public StatsServiceImpl() {
        winsByPlayerOne = new AtomicInteger();
        winsByPlayerTwo = new AtomicInteger();
        draws = new AtomicInteger();
    }

    @Override
    public int getWinsByPlayerOne() {
        return winsByPlayerOne.get();
    }

    @Override
    public int getWinsByPlayerTwo() {
        return winsByPlayerTwo.get();
    }

    @Override
    public int getDraws() {
        return draws.get();
    }

    @Override
    public void register(Round round) {
        switch (round.getResult()) {
            case DRAW:
                draws.incrementAndGet();
                break;
            case PLAYER1WIN:
                winsByPlayerOne.incrementAndGet();
                break;
            case PLAYER2WIN:
                winsByPlayerTwo.incrementAndGet();
                break;
        }
    }

    @Override
    public void reset() {
        winsByPlayerOne = new AtomicInteger();
        winsByPlayerTwo = new AtomicInteger();
        draws = new AtomicInteger();
    }

}
