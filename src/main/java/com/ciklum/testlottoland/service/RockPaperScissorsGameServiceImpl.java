package com.ciklum.testlottoland.service;

import com.ciklum.testlottoland.model.RockPaperScissorsOption;
import com.ciklum.testlottoland.model.Round;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class RockPaperScissorsGameServiceImpl implements GameService<RockPaperScissorsOption, Round> {

    @Autowired
    public RockPaperScissorsGameServiceImpl() {
    }

    @Override
    public Round play(RockPaperScissorsOption option1, RockPaperScissorsOption option2) {
        return new Round(option1, option2);
    }

    @Override
    public List<RockPaperScissorsOption> getOptions() {
        return Arrays.asList(RockPaperScissorsOption.values());
    }

}
