package com.ciklum.testlottoland.service;

public interface StatsService<T> {

    default int getTotalPlays() {
        return getWinsByPlayerOne() + getWinsByPlayerTwo() + getDraws();
    }

    int getWinsByPlayerOne();

    int getWinsByPlayerTwo();

    int getDraws();

    void register(T round);

    void reset();
}
